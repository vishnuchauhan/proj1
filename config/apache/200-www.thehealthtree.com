<VirtualHost *:80>
    ServerAdmin root@www.thehealthtree.com
    ServerName www.thehealthtree.com
    ServerSignature Off

    #Alias /static/ /home/webadmin/testproject/app/webapp/static/
    #Alias /robots.txt /home/webadmin/testproject/app/webapp/static/robots.txt
    #Alias /favicon.ico /home/webadmin/testproject/app/webapp/static/favicon.ico

    Alias /static/ /home/webadmin/ecomstore/static/
    #Alias /robots.txt /home/webadmin/ecomstore/static/robots/robots.txt
    Alias /favicon.ico /home/webadmin/ecomstore/static/ico/favicon.ico

    SetEnvIf User_Agent "monit/*" dontlog
    CustomLog "|/usr/sbin/rotatelogs /home/webadmin/ecomstore/logs/access.log.%Y%m%d-%H%M 5M" combined env=!dontlog
    ErrorLog "|/usr/sbin/rotatelogs /home/webadmin/ecomstore/logs/error.log.%Y%m%d-%H%M 5M"
    LogLevel warn

    WSGIScriptAlias / /home/webadmin/ecomstore/apache/wsgi.py

    #WSGIDaemonProcess www.thehealthtree.com user=webadmin group=webadmin processes=2 threads=10 maximum-requests=10000 display-name=%{GROUP} python-path=/home/webadmin/testproject/app:/home/webadmin/testproject/venv/lib/python2.7/site-packages python-eggs=/home/webadmin/testproject/run/eggs
    #WSGIProcessGroup www.thehealthtree.com
    WSGIDaemonProcess www.thehealthtree.com user=webadmin group=webadmin processes=2 threads=10 maximum-requests=10000 display-name=%{GROUP} python-path=/home/webadmin/ecomstore:/home/webadmin/virtualenvs/ecomstore/lib/python2.7/site-packages python-eggs=/home/webadmin/ecomstore/apache/run/eggs
    WSGIProcessGroup www.thehealthtree.com

    WSGIScriptAlias / /home/webadmin/ecomstore/apache/wsgi.py

    <Directory /home/webadmin/ecomstore/static>
        Order deny,allow
        Allow from all
        Options -Indexes FollowSymLinks
    </Directory>

    <Directory /home/webadmin/ecomstore/apache>
        Order deny,allow
        Allow from all
    </Directory>

 </VirtualHost>
