"""
WSGI config for webapp project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/howto/deployment/wsgi/
"""

#import os
#os.environ.setdefault("DJANGO_SETTINGS_MODULE", "webapp.settings")

#from django.core.wsgi import get_wsgi_application
#application = get_wsgi_application()

import os, sys

activate_this = '/home/webadmin/virtualenvs/ecomstore/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "../../")))
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "../")))

# path to directory of the .wsgi file ('apache/')
wsgi_dir = os.path.abspath(os.path.dirname(__file__))

# path to project root directory (parent of 'apache/')
project_dir = os.path.dirname(wsgi_dir)

#changing path
sys.path.append(project_dir)

#add the settings.py file to your system's PATH
project_settings = os.path.join(project_dir,'settings')

#explicitly define the DJANGO_SETTINGS_MODULE 
#os.environ['DJANGO_SETTINGS_MODULE'] = 'settings.settings'
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings.settings")

#print sys.path
#import django.core.handlers.wsgi
#application = django.core.handlers.wsgi.WSGIHandler()

#import djcelery
#setting up celery
#djcelery.setup_loader()
#CELERY_RESULT_BACKEND = "django://"
#CELERY_IMPORTS = ("apps.utils.tasks")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

