import os, sys


if 0:
    os.environ['PYTHON_EGG_CACHE'] = '/Users/santpald/ecomstore/apache/eggs'
    activate_this = '/Users/no_longer_susti/Desktop/Work/virtualenvs/ecomstore/bin/activate_this.py'
else:
    os.environ['PYTHON_EGG_CACHE'] = '/Users/no_longer_susti/Desktop/Work/ecomstore/apache/eggs'
    activate_this = '/Users/santpald/virtualenvs/ecomstore/bin/activate_this.py'


execfile(activate_this, dict(__file__=activate_this))

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "../../")))
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "../")))

# path to directory of the .wsgi file ('apache/')
wsgi_dir = os.path.abspath(os.path.dirname(__file__))

# path to project root directory (parent of 'apache/')
project_dir = os.path.dirname(wsgi_dir)

#changing path
sys.path.append(project_dir)

#add the settings.py file to your system's PATH
project_settings = os.path.join(project_dir,'settings')

#explicitly define the DJANGO_SETTINGS_MODULE 
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings.settings'

print sys.path

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()

